# -*- coding: utf-8 -*-

from twython import Twython
from datetime import datetime
import json
import os.path
import sys

# declare version
version = "0.1"

# exit if argv[1] is not specified
if len(sys.argv) < 2:
    print("[ERROR] No search term specified.")
    exit()

# saves search term
search_term = sys.argv[1]

# check if -v is given
if search_term == "-v":
    print("Version " + version)
    exit()

# open key file or exit if file does not exist
try:
    key_file = open("keys.json", "r")
except FileNotFoundError as error:
    print("[ERROR] Missing key file")
    exit()

keys = json.loads(key_file.read())

# exit if keys are incomplete
if len(keys) != 4:
    print("[ERROR] Invalid key file. Make sure that all 4 needed keys are included and seperated by line breaks.")
    exit()

# check if each key is at least 20 chars long
for key, value in keys.items():
    if len(value) < 20:
        print("[ERROR] Your key file contains at least one invalid key. Please reconfigure it.")
        exit()

# map keys and create Twitter client
CONSUMER_KEY = keys["CONSUMER_KEY"]
CONSUMER_SECRET = keys["CONSUMER_SECRET"]
ACCESS_KEY = keys["ACCESS_KEY"]
ACCESS_SECRET = keys["ACCESS_SECRET"]

twitter_client = Twython(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_KEY, ACCESS_SECRET)

# pagefile: file which contains the current page pointer (1, 2, ...)
if not os.path.isfile("page"):
    pagefile = open("page", "x")
    pagefile.write("1")
    pagefile.close()

pagefile = open("page")
p = int(pagefile.read())
pagefile.close()

# Twitter search for users
# q = query; count = number of users to return (max: 20); page = desired page of results
users = twitter_client.search_users(q = search_term, count = 20, page = p)
for user in users:
    print("[BLOCK] Time: " + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + " | Handle: @" + str(user["screen_name"]) + " | Display Name: " + user["name"] + " | ID: " + str(user["id"]))
    twitter_client.create_block(screen_name = user["screen_name"])

# increment page pointer
pagefile = open("page", "w")
pagefile.write(str(p + 1))
pagefile.close()

# YEEHAH
print("\nDone.")
exit()