# CrossBlockr/XBlockr

Ein Python-Skript zum automatisierten Blockieren von Nutzern, die einen bestimmten Term im Namen beinhalten.

## Motivation

Ursprünglich war CrossBlockr als Skript zum automatisierten Blockieren sogenannter "Kreuz-Accounts" auf Twitter vorgesehen. Da die API es jedoch nicht ermöglicht, bei einer simplen User-Suche Emojis zu verwenden (auch nicht, wenn diverse Codierungen verwendet werden), wurde das Tool soweit entkoppelt, dass ein beliebiger Term als Argument übergeben werden kann.  
Es wird empfohlen, CrossBlockr in Kombination mit einem UNIX-Cronjob (oder einer Windows-Aufgabe) zu verwenden.

## Vorbereitungen

Um CrossBlockr nutzen zu können, müssen folgende Vorbereitungen getroffen werden:  
* Installation von Python 3.6 (sofern noch nicht vorhanden)
* Installation von Twython mittels `pip`:  
`pip install twython`
* Anfordern eines [Twitter-Developer-Accounts](https://apps.twitter.com "Twitter Developer Dashboard") und Anlegen einer **eigenen** App mit Schreib- und Leserechten für den eigenen Account (das ist inzwischen ziemlich kompliziert, da man ne ganze Menge angeben muss)
* Generation von API Tokens im Twitter Developer Dashboard

Nach diesen Vorkehrungen kann CrossBlockr verwendet werden.

## Setup

Zunächst müssen die API-Keys in einer Datei namens `keys.json` gespeichert werden. Diese muss sich im gleichen Verzeichnis wie die Datei `xblockr.py` befinden und folgende Struktur aufweisen (die Anzahl der x stimmt nicht zwangsweise mit der tatsächlichen Anzahl an Zeichen überein):

```json
{
    "CONSUMER_KEY": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
    "CONSUMER_SECRET": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
    "ACCESS_KEY": "xxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxx",
    "ACCESS_SECRET": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}
```

Diese Datei kann manuell angelegt werden. Die Formatierung spielt dabei keine Rolle. Alternativ steht ein weiteres Skript zur Verfügung, welches die Datei automatisch erstellt. Es kann mit `python config.py` ausgeführt werden und fragt dann nacheinander nach den 4 Keys.

## Verwendung

### Syntaxbeschreibung

`python xblockr.py [-v | <TERM>]`

### Ausgabe der Version

Um die Version des Skripts auszugeben, kann der Parameter `-v` verwendet werden. Der Aufruf sieht dann also wie folgt aus:  
`python xblockr.py -v`

### Automatisches Suchen nach und Blockieren von Nutzern

Der Suchterm, nach dem gesucht und blockiert werden soll, wird direkt als Argument übergeben. Sollen beispielsweise alle Nutzer blockiert werden, deren Name oder Handle (Username) "QFD" beinhaltet, sieht der Aufruf wie folgt aus:  
`python xblockr.py QFD`

## Bekannte Probleme und geplante Erweiterungen

Momentan wird keine Logfile angelegt, sodass man nicht nachverfolgen kann, welche Accounts blockiert wurden. Weiterhin ist es nicht problemlos möglich, CrossBlockr mit mehr als einem Term zu verwenden. Auch die Fehlerbehandlung ist momentan recht rudimentär.

### Limitierungen durch die API

* Es werden insgesamt nur die ersten 1000 Accounts zurückgegeben
* Pro Durchlauf werden nur 20 Accounts zurückgegeben (und blockiert)
* Weiterhin sind pro Stunde nur n Anfragen erlaubt. Wie groß n ist, ist scheinbar nicht einheitlich, daher wird empfohlen, CrossBlockr nicht häufiger als alle 5 Minuten auszuführen.
* Es ist, wie bereits erwähnt, nicht möglich, nach Emojis zu suchen. Sollte der Grund für die Verwendung dieses Tools das Blockieren von ❌-Accounts sein, so ist "QFD" eine gute Alternative, wenn auch eine mit größerem Overblocking (das heißt es werden auch viele Accounts blockiert, die anderweitig etwas mit "QFD" zu tun haben).