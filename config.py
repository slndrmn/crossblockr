# -*- coding: utf-8 -*-

# Helps creating the keys.json file.

import json

# asks for API keys
CONSUMER_KEY = input("Enter Consumer Key: ")
CONSUMER_SECRET = input("Enter Consumer Secret: ")
ACCESS_KEY = input("Enter Access Key: ")
ACCESS_SECRET = input("Enter Access Secret: ")

# API token dictionary
keys = {
    "CONSUMER_KEY": CONSUMER_KEY,
    "CONSUMER_SECRET": CONSUMER_SECRET,
    "ACCESS_KEY": ACCESS_KEY,
    "ACCESS_SECRET": ACCESS_SECRET
}

# format and write into keys.json
keyfile = open("keys.json", "w")
keyfile.write(json.dumps(keys, indent = 4))
print("\nDone!")